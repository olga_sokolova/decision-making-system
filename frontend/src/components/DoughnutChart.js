import {Doughnut, mixins} from "vue-chartjs";
const {reactiveProp} = mixins;

export default {
    "extends": Doughnut,
    "mixins": [reactiveProp],
    "data" () {

        return {
            "options": {
                "title": {
                    "text": "Сравнение альтернатив между собой",
                    "display": true,
                    "fontColor": "rgba(255, 255, 255, 1)",
                    "fontSize": 17
                },
                "legend": {
                    "display": true,
                    "labels": {
                        "fontColor": "rgba(255, 255, 255, 1)",
                        "fontSize": 15
                    }
                },
                "responsive": true,
                "maintainAspectRatio": false,
                "height": 200
            }
        };

    },
    mounted () {

        this.renderChart(
            this.chartData,
            this.options
        );

    }
};
