import {Bar, mixins} from "vue-chartjs";
const {reactiveProp} = mixins;

export default {
    "extends": Bar,
    "mixins": [reactiveProp],
    "data" () {

        return {
            "options": {
                "title": {
                    "text": "Сравнение альтернатив по всем критериям",
                    "display": true,
                    "fontColor": "rgba(255, 255, 255, 1)",
                    "fontSize": 17
                },
                "scales": {
                    "yAxes": [
                        {
                            "ticks": {
                                "beginAtZero": true,
                                "fontColor": "rgba(255, 255, 255, 1)"
                            },
                            "gridLines": {
                                "display": true,
                                "color": "rgba(255, 255, 255, 0.2)"
                            }
                        }
                    ],
                    "xAxes": [
                        {
                            "ticks": {
                                "fontColor": "rgba(255, 255, 255, 1)",
                                "fontSize": 15,
                                "beginAtZero": true
                            },
                            "gridLines": {
                                "display": false
                            }
                        }
                    ]
                },
                "legend": {
                    "display": true,
                    "labels": {
                        "fontColor": "rgba(255, 255, 255, 1)",
                        "fontSize": 15
                    }
                },

                "responsive": true,
                "maintainAspectRatio": false,
                "height": 200
            }
        };

    },
    mounted () {

        this.renderChart(
            this.chartData,
            this.options
        );

    }
};
