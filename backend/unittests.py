import unittest
import kogger_alg
import analyzer
import json
import coefficient_dict
import pymorphy2
from app import views
from app import app


class MyTestCase(unittest.TestCase):
    def test_convert(self):
        self.assertEqual(coefficient_dict.getWordByCoef(8), "очень сильно превосходит")

    def test_reconvert(self):
        self.assertEqual(coefficient_dict.getCoefByWord('абсолютно превосходит'), 16)

    def test_get_questions(self):
        koggerRealisation = kogger_alg.KoggerRealization()
        koggerRealisation.wordsAnalyzer = analyzer.WordsAnalyzer()
        koggerRealisation.features_quantity = 2
        koggerRealisation.alternatives_quantity = 2
        koggerRealisation.features_names = ['скорость', 'стоимость']
        koggerRealisation.alternatives_names = ['автомобиль', 'велосипед']
        assert koggerRealisation.get_questions(kogger_alg.Subject.FEATURE, None)[0] in [
            "Оцените, насколько скорость для Вас важнее стоимости",
            "Для вас скорость важнее стоимости? Насколько?",
            "Дайте оценку: насколько скорость важнее для вас, чем стоимость"]

    def test_get_list_of_alternatives_questions(self):
        koggerRealisation = kogger_alg.KoggerRealization()
        koggerRealisation.wordsAnalyzer = analyzer.WordsAnalyzer()
        koggerRealisation.features_quantity = 2
        koggerRealisation.alternatives_quantity = 2
        koggerRealisation.features_names = ['скорость', 'стоимость']
        koggerRealisation.alternatives_names = ['автомобиль', 'велосипед']
        assert json.loads(koggerRealisation.get_list_of_alternatives_questions([2]))['a_question'][0][0] in [
            "Оцените, насколько скорость автомобиля для Вас предпочтительнее скорости велосипеда",
            "Насколько по вашему мнению автомобиль превосходит велосипед по скорости",
            "Для Вас скорость автомобиля предпочтительнее скорости велосипеда? Насколько?"]
        assert json.loads(koggerRealisation.get_list_of_alternatives_questions([2]))['a_question'][1][0] in [
            "Оцените, насколько стоимость автомобиля для Вас предпочтительнее стоимости велосипеда",
            "Насколько по вашему мнению автомобиль превосходит велосипед по стоимости",
            "Для Вас стоимость автомобиля предпочтительнее стоимости велосипеда? Насколько?"]

    def test_get_list_of_alternatives_questions_many_features(self):
        koggerRealisation = kogger_alg.KoggerRealization()
        koggerRealisation.wordsAnalyzer = analyzer.WordsAnalyzer()
        koggerRealisation.features_quantity = 7
        koggerRealisation.alternatives_quantity = 2
        koggerRealisation.features_names = ['скорость', 'стоимость', 'безопасность', 'дизайн', 'удобство', 'страховка',
                                            'производитель']
        koggerRealisation.alternatives_names = ['автомобиль', 'велосипед']
        assert json.loads(koggerRealisation.get_list_of_alternatives_questions([2, 2, 4, 2, 1, 1]))['a_question'][0][
                   0] in [
                   "Оцените, насколько скорость автомобиля для Вас предпочтительнее скорости велосипеда",
                   "Насколько по вашему мнению автомобиль превосходит велосипед по скорости",
                   "Для Вас скорость автомобиля предпочтительнее скорости велосипеда? Насколько?"]
        assert json.loads(koggerRealisation.get_list_of_alternatives_questions([2, 2, 4, 2, 1, 1]))['a_question'][1][
                   0] in [
                   "Оцените, насколько стоимость автомобиля для Вас предпочтительнее стоимости велосипеда",
                   "Насколько по вашему мнению автомобиль превосходит велосипед по стоимости",
                   "Для Вас стоимость автомобиля предпочтительнее стоимости велосипеда? Насколько?"]
        assert json.loads(koggerRealisation.get_list_of_alternatives_questions([2, 2, 4, 2, 1, 1]))['a_question'][2][
                   0] in [
                   "Оцените, насколько безопасность автомобиля для Вас предпочтительнее безопасности велосипеда",
                   "Насколько по вашему мнению автомобиль превосходит велосипед по безопасности",
                   "Для Вас безопасность автомобиля предпочтительнее безопасности велосипеда? Насколько?"]

    def test_get_list_of_features_questions(self):
        koggerRealisation = kogger_alg.KoggerRealization()
        koggerRealisation.wordsAnalyzer = analyzer.WordsAnalyzer()
        koggerRealisation.features_quantity = 2
        koggerRealisation.alternatives_quantity = 2
        koggerRealisation.features_names = ['скорость', 'стоимость']
        koggerRealisation.alternatives_names = ['автомобиль', 'велосипед']
        assert json.loads(koggerRealisation.get_list_of_features_questions())['f_question'][0] in [
            "Оцените, насколько скорость для Вас важнее стоимости",
            "Для вас скорость важнее стоимости? Насколько?",
            "Дайте оценку: насколько скорость важнее для вас, чем стоимость"]

    def test_see_next(self):
        koggerRealisation = kogger_alg
        self.assertEqual(koggerRealisation.see_next(1, ['Согласен', 'Не согласен', 'Не согласен', 'Согласен']), 2)

    def test_get_combinations(self):
        self.assertEqual(coefficient_dict.get_combinations(2), [[16, 0.125], [8, 0.25],
                                                             [4, 0.5], [2, 1], [1, 2], [0.125, 16],
                                                             [0.25, 8], [0.5, 4]])

    def test_get_weights_list(self):
        koggerRealisation = kogger_alg
        self.assertEqual(koggerRealisation.get_weights_list([1, 2])[0], 0.4)
        self.assertEqual(koggerRealisation.get_weights_list([1, 2])[1], 0.4)
        self.assertEqual(koggerRealisation.get_weights_list([1, 2])[2], 0.2)

    def test_get_best_alternative_djoffrion(self):
        koggerRealisation = kogger_alg.KoggerRealization()
        koggerRealisation.wordsAnalyzer = analyzer.WordsAnalyzer()
        koggerRealisation.features_quantity = 2
        koggerRealisation.alternatives_quantity = 2
        koggerRealisation.answers_about_features = [2]
        koggerRealisation.answers_about_alternatives = [[1], [2]]
        koggerRealisation.features_names = ['скорость', 'стоимость']
        koggerRealisation.alternatives_names = ['автомобиль', 'велосипед']
        self.assertEqual(json.loads(koggerRealisation.get_best_alternative_djoffrion())['best'], 0)
        self.assertEqual(json.loads(koggerRealisation.get_best_alternative_djoffrion())['all_datasets'],
                         [[0.5, 0.6666666666666666], [0.5, 0.3333333333333333]])
        self.assertEqual(json.loads(koggerRealisation.get_best_alternative_djoffrion())['functional'],
                         [0.2222222222222222, 0.1111111111111111])
        self.assertEqual(json.loads(koggerRealisation.get_best_alternative_djoffrion())['indexes'],
                         ['скорость', 'стоимость'])

    def test_get_best_alternative_djoffrion_many_features(self):
        koggerRealisation = kogger_alg.KoggerRealization()
        koggerRealisation.wordsAnalyzer = analyzer.WordsAnalyzer()
        koggerRealisation.features_quantity = 3
        koggerRealisation.alternatives_quantity = 2
        koggerRealisation.answers_about_features = [2, 2, 4, 2, 1, 1]
        koggerRealisation.answers_about_alternatives = [[1], [2], [2]]
        koggerRealisation.features_names = ['скорость', 'стоимость', 'безопасность']
        koggerRealisation.alternatives_names = ['автомобиль', 'велосипед']
        koggerRealisation.weights_of_max_features = [0.5245901639344263, 0.26229508196721313, 0.13114754098360656]
        koggerRealisation.indexes_of_max_features = [0, 1, 2]

        self.assertEqual(json.loads(koggerRealisation.get_best_alternative_djoffrion())['best'], 0)
        self.assertEqual(json.loads(koggerRealisation.get_best_alternative_djoffrion())['all_datasets'],
                         [[0.5, 0.6666666666666666, 0.6666666666666666], [0.5, 0.3333333333333333, 0.3333333333333333]])
        self.assertEqual(json.loads(koggerRealisation.get_best_alternative_djoffrion())['functional'],
                         [0.08743169398907104, 0.04371584699453552])
        self.assertEqual(json.loads(koggerRealisation.get_best_alternative_djoffrion())['indexes'],
                         ['скорость', 'стоимость', 'безопасность'])

    def test_get_additional_question_many_features(self):
        koggerRealisation = kogger_alg.KoggerRealization()
        koggerRealisation.wordsAnalyzer = analyzer.WordsAnalyzer()
        koggerRealisation.features_quantity = 3
        koggerRealisation.alternatives_quantity = 2
        koggerRealisation.answers_about_features = [2, 2, 4, 2, 1, 1]
        koggerRealisation.answers_about_alternatives = [[1], [2], [2]]
        koggerRealisation.features_names = ['скорость', 'стоимость', 'безопасность']
        koggerRealisation.alternatives_names = ['автомобиль', 'велосипед']
        koggerRealisation.weights_of_max_features = [0.5245901639344263, 0.26229508196721313, 0.13114754098360656]
        koggerRealisation.indexes_of_max_features = [0, 1, 2]
        assert json.loads(koggerRealisation.get_additional_question())['f_question'][0] in [
            "Оцените, насколько скорость для Вас важнее безопасности",
            "Для вас скорость важнее безопасности? Насколько?",
            "Дайте оценку: насколько скорость важнее для вас, чем безопасность"]

    def test_get_additional_question(self):
        koggerRealisation = kogger_alg.KoggerRealization()
        koggerRealisation.wordsAnalyzer = analyzer.WordsAnalyzer()
        koggerRealisation.features_quantity = 3
        koggerRealisation.alternatives_quantity = 2
        koggerRealisation.answers_about_features = [2, 2]
        koggerRealisation.answers_about_alternatives = [[1], [2], [8]]
        koggerRealisation.features_names = ['скорость', 'стоимость', 'безопасность']
        koggerRealisation.alternatives_names = ['автомобиль', 'велосипед']
        assert json.loads(koggerRealisation.get_additional_question())['f_question'][0] in [
            "Оцените, насколько скорость для Вас важнее безопасности",
            "Для вас скорость важнее безопасности? Насколько?",
            "Дайте оценку: насколько скорость важнее для вас, чем безопасность"]

    def test_process_additional_answers_no_errors(self):
        koggerRealisation = kogger_alg.KoggerRealization()
        koggerRealisation.wordsAnalyzer = analyzer.WordsAnalyzer()
        koggerRealisation.features_quantity = 3
        koggerRealisation.alternatives_quantity = 2
        koggerRealisation.answers_about_features = [2, 2]
        koggerRealisation.answers_about_alternatives = [[1], [2], [8]]
        koggerRealisation.features_names = ['скорость', 'стоимость', 'безопасность']
        koggerRealisation.alternatives_names = ['автомобиль', 'велосипед']
        koggerRealisation.additional_features_answers = [8]
        self.assertEqual(koggerRealisation.process_additional_answers(['очень сильно превосходит']), 'ok')

    def test_process_additional_answers_are_errors(self):
        koggerRealisation = kogger_alg.KoggerRealization()
        koggerRealisation.wordsAnalyzer = analyzer.WordsAnalyzer()
        koggerRealisation.features_quantity = 3
        koggerRealisation.alternatives_quantity = 2
        koggerRealisation.answers_about_features = [2, 2]
        koggerRealisation.answers_about_alternatives = [[1], [2], [8]]
        koggerRealisation.features_names = ['скорость', 'стоимость', 'безопасность']
        koggerRealisation.alternatives_names = ['автомобиль', 'велосипед']
        koggerRealisation.additional_features_answers = [8]
        self.assertEqual(json.loads(koggerRealisation.process_additional_answers(['сильно превосходит']))[0]['number'],
                         0)
        self.assertEqual(json.loads(koggerRealisation.process_additional_answers(['сильно превосходит']))[0]['user'],
                         'скорость сильно превосходит безопасность')
        self.assertEqual(json.loads(koggerRealisation.process_additional_answers(['сильно превосходит']))[0]['system'],
                         'скорость очень сильно превосходит безопасность')

    def test_process_errors(self):
        koggerRealisation = kogger_alg.KoggerRealization()
        koggerRealisation.wordsAnalyzer = analyzer.WordsAnalyzer()
        koggerRealisation.features_quantity = 3
        koggerRealisation.alternatives_quantity = 2
        koggerRealisation.answers_about_features = [2, 2]
        koggerRealisation.answers_about_alternatives = [[1], [2], [8]]
        koggerRealisation.features_names = ['скорость', 'стоимость', 'безопасность']
        koggerRealisation.alternatives_names = ['автомобиль', 'велосипед']
        koggerRealisation.additional_features_answers = [8]
        koggerRealisation.information_about_errors = [{"number": 0, "user": 4, "system": 8}]
        out = [['скорость абсолютно превосходит стоимость', 'стоимость сильно уступает безопасности'],
               ['скорость очень сильно превосходит стоимость', 'стоимость слабо уступает безопасности'],
               ['скорость сильно превосходит стоимость', 'стоимость и безопасность равнозначны'],
               ['скорость слабо превосходит стоимость', 'стоимость слабо превосходит безопасность'],
               ['скорость и стоимость равнозначны', 'стоимость сильно превосходит безопасность'],
               ['скорость сильно уступает стоимости', 'стоимость абсолютно превосходит безопасность'],
               ['скорость слабо уступает стоимости', 'стоимость очень сильно превосходит безопасность']]
        self.assertEqual(json.loads(koggerRealisation.process_errors(['Не согласен']))['e_question'][0], out)

    def test_process_spec_answers(self):
        koggerRealisation = kogger_alg.KoggerRealization()
        koggerRealisation.wordsAnalyzer = analyzer.WordsAnalyzer()
        koggerRealisation.features_quantity = 3
        koggerRealisation.alternatives_quantity = 2
        koggerRealisation.answers_about_features = [2, 2]
        koggerRealisation.answers_about_alternatives = [[1], [2], [8]]
        koggerRealisation.features_names = ['скорость', 'стоимость', 'безопасность']
        koggerRealisation.alternatives_names = ['автомобиль', 'велосипед']
        koggerRealisation.additional_features_answers = [8]
        koggerRealisation.information_about_errors = [{"number": 0, "user": 4, "system": 8}]
        koggerRealisation.spec_questions = [0]
        koggerRealisation.combinations = [coefficient_dict.get_combinations(4)]
        koggerRealisation.features_answers = [2, 2]
        koggerRealisation.choose = ['user']
        koggerRealisation.process_spec_answers([0])
        self.assertEqual(koggerRealisation.features_answers, [16, 0.25])

    def test_process_errors_only_agrees(self):
        """
        на process_errors(self, error_answers), принимающий массив из элементов "Согласен", "Не согласен"
        и возвращающий список уточняющих вопросов
        """
        koggerRealisation = kogger_alg.KoggerRealization()
        koggerRealisation.wordsAnalyzer = analyzer.WordsAnalyzer()
        koggerRealisation.features_quantity = 3
        koggerRealisation.alternatives_quantity = 2
        koggerRealisation.answers_about_features = [2, 2]
        koggerRealisation.answers_about_alternatives = [[1], [2], [8]]
        koggerRealisation.features_names = ['скорость', 'стоимость', 'безопасность']
        koggerRealisation.alternatives_names = ['автомобиль', 'велосипед']
        koggerRealisation.additional_features_answers = [8]
        koggerRealisation.information_about_errors = [{"number": 0, "user": 4, "system": 8}]
        koggerRealisation.process_errors(['Cогласен'])
        self.assertEqual(len(json.loads(koggerRealisation.process_errors(['Согласен']))['e_question']), 0)

    def test_process_errors_with_agrees(self):
        """
        на process_errors(self, error_answers), принимающий массив из элементов "Согласен", "Не согласен"
        и возвращающий список уточняющих вопросов
        """
        koggerRealisation = kogger_alg.KoggerRealization()
        koggerRealisation.wordsAnalyzer = analyzer.WordsAnalyzer()
        koggerRealisation.features_quantity = 4
        koggerRealisation.alternatives_quantity = 2
        koggerRealisation.answers_about_features = [2, 2, 1]
        koggerRealisation.answers_about_alternatives = [[1], [2], [8], [4]]
        koggerRealisation.features_names = ['скорость', 'стоимость', 'безопасность', 'комфорт']
        koggerRealisation.alternatives_names = ['автомобиль', 'велосипед']
        koggerRealisation.additional_features_answers = [8]
        koggerRealisation.information_about_errors = [{"number": 0, "user": 4, "system": 8},
                                                      {"number": 1, "user": 4, "system": 8}]
        out = [['скорость абсолютно превосходит стоимость', 'стоимость сильно уступает безопасности'],
               ['скорость очень сильно превосходит стоимость', 'стоимость слабо уступает безопасности'],
               ['скорость сильно превосходит стоимость', 'стоимость и безопасность равнозначны'],
               ['скорость слабо превосходит стоимость', 'стоимость слабо превосходит безопасность'],
               ['скорость и стоимость равнозначны', 'стоимость сильно превосходит безопасность'],
               ['скорость сильно уступает стоимости', 'стоимость абсолютно превосходит безопасность'],
               ['скорость слабо уступает стоимости', 'стоимость очень сильно превосходит безопасность']]
        self.assertEqual(json.loads(koggerRealisation.process_errors(['Не согласен', 'Согласен']))['e_question'][0],
                         out)

    def test_process_spec_answers_many_features(self):
        koggerRealisation = kogger_alg.KoggerRealization()
        koggerRealisation.wordsAnalyzer = analyzer.WordsAnalyzer()
        koggerRealisation.features_quantity = 3
        koggerRealisation.alternatives_quantity = 2
        koggerRealisation.answers_about_features = [2, 2, 4, 2, 1, 1]
        koggerRealisation.answers_about_alternatives = [[1], [2], [2]]
        koggerRealisation.features_names = ['скорость', 'стоимость', 'безопасность']
        koggerRealisation.alternatives_names = ['автомобиль', 'велосипед']
        koggerRealisation.weights_of_max_features = [0.5245901639344263, 0.26229508196721313, 0.13114754098360656]
        koggerRealisation.indexes_of_max_features = [0, 1, 2]
        koggerRealisation.additional_features_answers = [8]
        koggerRealisation.information_about_errors = [{"number": 0, "user": 4, "system": 8}]
        koggerRealisation.spec_questions = [0]
        koggerRealisation.combinations = [coefficient_dict.get_combinations(4)]
        koggerRealisation.choose = ['user']
        koggerRealisation.process_spec_answers([0])
        self.assertEqual(koggerRealisation.features_answers, [16, 0.25])

    def test_get_word_on_the_case(self):
        wordsAnalyzer = analyzer.WordsAnalyzer()
        wordsAnalyzer.morph = pymorphy2.MorphAnalyzer()
        self.assertEqual(wordsAnalyzer.get_word_on_the_case('small lamp', 'gent'), 'small lamp')
        self.assertEqual(wordsAnalyzer.get_word_on_the_case('маленькая лампа', 'gent'), 'маленькой лампы')
        self.assertEqual(wordsAnalyzer.get_word_on_the_case('подставка для лампы', 'gent'), 'подставки для лампы')
        self.assertEqual(wordsAnalyzer.get_word_on_the_case('Подставка для лампы', 'gent'), 'Подставки для лампы')
        self.assertEqual(wordsAnalyzer.get_word_on_the_case('Mаленькая лампа', 'gent'), 'Mаленькой лампы')
        self.assertEqual(wordsAnalyzer.get_word_on_the_case('лампа', 'gent'), 'лампы')
        self.assertEqual(wordsAnalyzer.get_word_on_the_case('маленькая белая лампа', 'gent'), 'маленькой белой лампы')
        self.assertEqual(wordsAnalyzer.get_word_on_the_case('маленькая лампа моей бабушки', 'gent'),
                         'маленькой лампы моей бабушки')

    # API
    def setUp(self):
        app.config['TESTING'] = True
        self.app = app.test_client()

    def test_index_names(self):
        result = self.app.get('/names?number_of_features=2&number_of_alternatives=2&'
                              'names_of_features=стоимость~~безопасность&'
                              'names_of_alternatives=автомобиль~~велосипед', content_type='application/json')
        self.assertEqual(200, result.status_code)
        assert json.loads(result.data)['f_question'][0] in [
            "Оцените, насколько стоимость для Вас важнее безопасности",
            "Для вас стоимость важнее безопасности? Насколько?",
            "Дайте оценку: насколько стоимость важнее для вас, чем безопасность"]

    def test_index_features_answers(self):
        kogger_realization = kogger_alg.KoggerRealization()
        kogger_realization.features_quantity = 2
        kogger_realization.alternatives_quantity = 2
        kogger_realization.features_names = ['стоимость', 'безопасность']
        kogger_realization.alternatives_names = ['велосипед', 'машина']
        views.koggerRealization = kogger_realization
        result = self.app.get('/features_answers?features_answers=1', content_type='application/json')
        self.assertEqual(200, result.status_code)
        assert json.loads(result.data)['a_question'][0][0] in [
            "Оцените, насколько стоимость велосипеда для Вас предпочтительнее стоимости машины",
            "Насколько по вашему мнению велосипед превосходит машину по стоимости",
            "Для Вас стоимость велосипеда предпочтительнее стоимости машины? Насколько?"]
        assert json.loads(result.data)['a_question'][1][0] in [
            "Оцените, насколько безопасность велосипеда для Вас предпочтительнее безопасности машины",
            "Насколько по вашему мнению велосипед превосходит машину по безопасности",
            "Для Вас безопасность велосипеда предпочтительнее безопасности машины? Насколько?"]


if __name__ == '__main__':
    unittest.main()
