dictionary = [[16, "абсолютно превосходит"],
              [8, "очень сильно превосходит"],
              [4, "сильно превосходит"],
              [2, "слабо превосходит"],
              [1, "равнозначны"],
              [0.5, "слабо уступает"],
              [0.25, "сильно уступает"],
              [0.125, "очень сильно уступает"]]

degreesOfBetter = [16, 8, 4, 2, 1, 0.0625, 0.125, 0.25, 0.5]
degreesOfWorse = [0.0625, 0.125, 0.25, 0.5, 16, 8, 4, 2, 1]


def getWordByCoef(coef):
    for pair in dictionary:
        if pair[0] == coef:
            return pair[1]
    raise RuntimeError('Error coefficient!')


def getCoefByWord(word):
    for pair in dictionary:
        if pair[1] == word:
            return pair[0]
    raise RuntimeError('Error word!')


def get_combinations(coef):
    """

    :param coef:
    :return:
    """
    combinations = []
    for i in degreesOfBetter:
        for j in degreesOfWorse:
            if i * j == coef:
                element = [i, j]
                combinations.append(element)
    return combinations
