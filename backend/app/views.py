"""API"""
from app import app
from flask import request
from flask_cors import CORS
from kogger_alg import KoggerRealization
import numpy as np

cors = CORS(app)
app.config['CORS_HEADERS'] = 'application/json'
koggerRealization = None


@app.route('/names')
@app.route('/index_names')
def index_names():
    """

    :return:
    """
    global koggerRealization
    koggerRealization = KoggerRealization()
    koggerRealization.features_quantity = int(request.args.get('number_of_features'))
    koggerRealization.alternatives_quantity = int(request.args.get('number_of_alternatives'))
    koggerRealization.features_names = list(request.args.get('names_of_features').split('~~'))
    koggerRealization.alternatives_names = list(request.args.get('names_of_alternatives').split('~~'))
    result = koggerRealization.get_list_of_features_questions()
    return result


@app.route('/features_answers')
@app.route('/index_features_answers')
def index_features_answers():
    """

    :return:
    """
    features_answers = list(request.args.get('features_answers').split(','))
    koggerRealization.answers_about_features = features_answers
    print(koggerRealization)
    result = koggerRealization.get_list_of_alternatives_questions(features_answers)
    return str(result)


@app.route('/alternatives_answers')
@app.route('/index_alternatives_answers')
def index_alternatives_answers():
    """

    :return:
    """
    answers = list(request.args.get('alternatives_answers').split(','))
    alternatives_answers = []
    for i in np.arange(0, len(answers), koggerRealization.alternatives_quantity - 1):
        alternatives_answers_for_feature = answers[i:i + koggerRealization.alternatives_quantity - 1]
        alternatives_answers.append(alternatives_answers_for_feature)
    koggerRealization.answers_about_alternatives = alternatives_answers
    return "200"


@app.route('/additional_questions')
@app.route('/index_additional_questions')
def index_additional_questions():
    """

    :return:
    """
    result = koggerRealization.get_additional_question()
    return str(result)


@app.route('/additional_answers')
@app.route('/index_additional_answers')
def index_additional_answers():
    """

    :return:
    """
    additional_features_answers = list(request.args.get('additional_features_answers').split(','))
    result = koggerRealization.process_additional_answers(additional_features_answers)
    return str(result)


@app.route('/error_answers')
@app.route('/index_error_answers')
def index_error_answers():
    """

    :return:
    """
    error_answers = list(request.args.get('error_answers').split(','))
    result = koggerRealization.process_errors(error_answers)
    return str(result)


@app.route('/spec_answers')
@app.route('/index_spec_answers')
def index_spec_answers():
    """

    :return:
    """
    spec_answers = list(request.args.get('spec_answers').split(','))
    result = koggerRealization.process_spec_answers(spec_answers)
    return str(result)


@app.route('/best_variant')
@app.route('/index_best_variant')
def index_best_variant():
    """

    :return:
    """
    res = koggerRealization.get_best_alternative_djoffrion()
    return str(res)
