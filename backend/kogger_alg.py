"""Kogger algorithm realization"""
import json
import numpy as np
from analyzer import WordsAnalyzer
import coefficient_dict
from enum import Enum


class Subject(Enum):
    FEATURE = 1
    ALTERNATIVE = 2

def see_next(current, error_answers):
    """

    :param current:
    :param error_answers:
    :return:
    """
    i = 0
    for num in np.arange(current, len(error_answers)):
        if error_answers[num] == "Не согласен":
            i = i + 1
        else:
            break
    return i


def get_weights_list(answers_list):
    """

    :param answers_list:
    :return:
    """
    weight_list = np.zeros(len(answers_list) + 1)
    weight_list[len(answers_list)] = 1
    parts = 1
    for i in range(len(answers_list), 0, -1):
        weight_list[i - 1] = weight_list[i] * float(answers_list[i - 1])
        parts = parts + weight_list[i - 1]

    for i in range(len(weight_list)):
        weight_list[i] = weight_list[i] / parts
    return weight_list


class KoggerRealization:
    """Kogger algorithm realization"""
    A_LABEL = "a_question"
    F_LABEl = "f_question"
    wordsAnalyzer = WordsAnalyzer()
    features_quantity = 0
    alternatives_quantity = 0
    features_names = []
    alternatives_names = []
    answers_about_features = []
    answers_about_alternatives = []
    weights_of_max_features = []
    indexes_of_max_features = []
    additional_features_answers = []
    additional_alternatives_answers = []
    additional_features_answers_user = []
    information_about_errors = []
    spec_questions = []
    combinations = []
    spec_questions_for_features = []
    error_features = []
    choose = []
    features_answers = []

    def get_questions(self, subject, current_feature_number):
        """

        :param subject:
        :param current_feature_number:
        :return:
        """
        question_features_list = []
        features_ij = np.zeros(self.features_quantity - 1)
        alternatives_ij = np.zeros(self.alternatives_quantity - 1)

        if subject == Subject.FEATURE:
            for i in range(len(features_ij)):
                question_features_list.append(
                    self.wordsAnalyzer.get_random_feature_question(self.features_names[i], self.features_names[i + 1]))
        elif subject == Subject.ALTERNATIVE:
            for i in range(len(alternatives_ij)):
                question_features_list.append(
                    self.wordsAnalyzer.get_random_alternative_question(self.alternatives_names[i],
                                                                       self.alternatives_names[i + 1],
                                                                       self.features_names[
                                                                           current_feature_number]))

        return question_features_list

    def get_list_of_alternatives_questions(self, features_answers):
        """

        :param features_answers:
        :return:
        """
        if self.features_quantity < 7:
            result = self.get_json()
        else:
            all_features = get_weights_list(features_answers)
            index_and_weights = {}
            for each in range(len(all_features)):
                index_and_weights.update({each: all_features[each]})

            sorted_index_and_weights = {}
            sorted_keys = sorted(index_and_weights, key=index_and_weights.get)
            for key in sorted_keys:
                sorted_index_and_weights[key] = index_and_weights[key]

            new_features_quantity = int(self.features_quantity / 2)
            max_indexes = list(sorted_index_and_weights.keys())[new_features_quantity + 1:]
            features_weights = list(sorted_index_and_weights.values())[new_features_quantity + 1:]

            max_indexes.sort()
            new_features_names = []
            for each in max_indexes:
                new_features_names.append(self.features_names[each])

            self.features_names = new_features_names
            self.features_quantity = new_features_quantity
            self.weights_of_max_features = features_weights
            self.indexes_of_max_features = max_indexes
            result = self.get_json()
        return result

    def get_json(self):
        question_alternatives_list_ = []
        for i in range(self.features_quantity):
            question_alternatives_list = self.get_questions(Subject.ALTERNATIVE, i)
            question_alternatives_list_.append(question_alternatives_list)
        result = ({self.A_LABEL: question_alternatives_list_})
        result = json.dumps(result)
        return result

    def get_list_of_features_questions(self):
        """

        :return:
        """
        question_features_list_ = self.get_questions(Subject.FEATURE, None)
        result = {self.F_LABEl: question_features_list_}
        result = json.dumps(result)
        return result

    def process_spec_answers(self, spec_answers):
        """

        :param spec_answers:
        :return:
        """
        if len(self.weights_of_max_features) == 0:  # это значит что количество критериев небольшое
            self.features_answers = self.answers_about_features
        else:
            i = 0
            while i < len(self.weights_of_max_features) - 1:
                self.features_answers.append(self.weights_of_max_features[i] / self.weights_of_max_features[i + 1])
                i = i + 1

        print(self.information_about_errors)
        print(self.additional_features_answers)
        i = 0
        while i < len(self.additional_features_answers):
            j = 0
            while j < len(self.information_about_errors):
                if i == self.information_about_errors[j]['number']:
                    self.additional_features_answers[i] = self.information_about_errors[j][self.choose[j]]
                j = j + 1
            i = i + 1

        print(self.additional_features_answers)

        print("BEFORE ", self.features_answers)

        self.features_answers[self.spec_questions[0]] = self.combinations[0][int(spec_answers[0])][0]
        self.features_answers[self.spec_questions[0] + 1] = self.combinations[0][int(spec_answers[0])][1]
        print("BEFORE ", self.features_answers)

        i = self.spec_questions[0] - 1
        while i >= 0:
            self.features_answers[i] = self.additional_features_answers[i] / self.features_answers[i + 1]
            i = i - 1

        i = self.spec_questions[0] + 2
        while i < len(self.features_answers):
            self.features_answers[i] = self.additional_features_answers[i - 1] / self.features_answers[i - 1]
            i = i + 1

        print("AFTER ", self.features_answers)
        return self.get_best_alternative_djoffrion()

    def process_errors(self, error_answers):
        """

        :param error_answers:
        :return:
        """
        combinations = []
        questions = []
        i = 0
        while i < len(error_answers):
            if error_answers[i] == "Не согласен":
                k = see_next(i, error_answers)
                question_number = i
                while question_number < i + k:
                    questions_i = []
                    current = self.information_about_errors[question_number]['number']
                    self.spec_questions.append(current)
                    combinations_i = coefficient_dict.get_combinations(
                        self.information_about_errors[question_number]['user'])
                    combinations.append(combinations_i)
                    for each in combinations_i:
                        if each[0] == 1:
                            question_1 = self.features_names[current] + " и " + self.features_names[current + 1] \
                                         + " " + coefficient_dict.getWordByCoef(each[0])
                        elif each[0] < 1:
                            question_1 = self.features_names[current] + " " + coefficient_dict.getWordByCoef(
                                each[0]) + " " + \
                                         self.wordsAnalyzer.get_word_on_the_case(self.features_names[current + 1],
                                                                                 'datv')
                        else:
                            question_1 = self.features_names[current] + " " + coefficient_dict.getWordByCoef(
                                each[0]) + " " + \
                                         self.features_names[current + 1]
                        if each[1] == 1:
                            question_2 = self.features_names[current + 1] + " и " + self.features_names[
                                current + 2] + " " + \
                                         coefficient_dict.getWordByCoef(each[1])
                        elif each[1] < 1:
                            question_2 = self.features_names[current + 1] + " " + coefficient_dict.getWordByCoef(
                                each[1]) + " " + \
                                         self.wordsAnalyzer.get_word_on_the_case(self.features_names[current + 2],
                                                                                 'datv')
                        else:
                            question_2 = self.features_names[current + 1] + " " + coefficient_dict.getWordByCoef(
                                each[1]) + " " + \
                                         self.features_names[current + 2]
                        questions_i.append([question_1, question_2])
                    questions.append(questions_i)
                    question_number += 2
                    break
                i = i + k
            else:
                i = i + 1

        spec_questions_for_features = []
        i = 0
        while i < len(error_answers):
            if error_answers[i] == "Не согласен":
                spec_questions_for_features.append(self.information_about_errors[i]['user'])
                self.choose.append('user')
            else:
                spec_questions_for_features.append(self.information_about_errors[i]['system'])
                self.choose.append('system')
            i = i + 1

        self.spec_questions_for_features = spec_questions_for_features
        self.combinations = combinations
        result = {"e_question": questions}
        result = json.dumps(result)
        return result

    def process_additional_answers(self, additional_features_answers):
        """

        :param additional_features_answers:
        :return:
        """
        self.additional_features_answers_user = additional_features_answers
        features_error = []

        for i in np.arange(0, len(additional_features_answers)):
            if additional_features_answers[i] != coefficient_dict.getWordByCoef(self.additional_features_answers[i]):
                user = self.features_names[i] + " " + additional_features_answers[i] + " " \
                       + self.features_names[i + 2]
                system = self.features_names[i] + " " + coefficient_dict.getWordByCoef(
                    self.additional_features_answers[i]) + " " \
                         + self.features_names[i + 2]
                features_error.append({"number": int(i), "user": user, "system": system})
                self.information_about_errors.append(
                    {"number": i, "user": coefficient_dict.getCoefByWord(additional_features_answers[i]),
                     "system": self.additional_features_answers[i]})
        if len(features_error) == 0:
            return "ok"
        result = json.dumps(features_error)
        return result

    def get_additional_question(self):
        """

        :return:
        """
        question_features_list = []
        if len(self.weights_of_max_features) == 0:  # это значит что количество критериев небольшое
            features_answers = self.answers_about_features
        else:
            i = 0
            features_answers = []
            while i < len(self.weights_of_max_features) - 1:
                features_answers.append(self.weights_of_max_features[i] / self.weights_of_max_features[i + 1])
                i = i + 1

        # if len(self.weights_of_max_features) == 0:  # это значит что количество критериев небольшое
        #     features_index_list = np.arange(0, self.features_quantity)
        # else:
        #     features_index_list = self.indexes_of_max_features

        additional_features_answers = []
        if len(features_answers) > 1:
            for i in np.arange(0, len(features_answers) - 1):
                additional_features_answers.append(float(features_answers[i]) * float(features_answers[i + 1]))

            k = 0
            for _ in additional_features_answers:
                question_features_list.append(
                    self.wordsAnalyzer.get_random_feature_question(self.features_names[k],
                                                                   self.features_names[k + 2]))
                k = k + 1

        additional_alternatives_answers = {}
        # j = 0
        # for feature_i in features_index_list:
        #     if len(self.answers_about_alternatives[j]) > 1:
        #         additional_alternatives_answers_feature_i = []
        #         for i in np.arange(0, len(self.answers_about_alternatives[j]) - 1):
        #             additional_alternatives_answers_feature_i.append(
        #                 float(self.answers_about_alternatives[j][i])
        #                 * float(self.answers_about_alternatives[j][i + 1]))
        #         additional_alternatives_answers.update({feature_i: additional_alternatives_answers_feature_i})
        #         j = j + 1
        #
        # print(additional_alternatives_answers)
        question_alternatives_list = []
        #
        # i = 0
        # for index in features_index_list:
        #     k = 0
        #     if len(self.answers_about_alternatives[i]) > 1:
        #         for each in additional_alternatives_answers[index]:
        #             question_alternatives_list.append(
        #                 self.wordsAnalyzer.get_random_alternative_question(self.alternatives_names[k],
        #                                                                    self.alternatives_names[
        #                                                                        k + 2],
        #                                                                    self.features_names[i]))
        #             k = k + 1
        #     i = i + 1
        # print(question_alternatives_list)

        self.additional_features_answers = additional_features_answers
        self.additional_alternatives_answers = additional_alternatives_answers

        result = {self.F_LABEl: question_features_list, self.A_LABEL: question_alternatives_list}
        result = json.dumps(result)
        print(result)
        return result

    # def get_best_alternative_linear(self):
    #     alternatives_answers = self.answers_about_alternatives
    #     self.get_additional_question()
    #     alternatives = []
    #
    #     if len(self.weights_of_max_features) == 0:  # это значит что количество критериев небольшое
    #         features_answers = self.answers_about_features
    #         features_i_ = self.get_weights_list(features_answers)
    #     else:
    #         features_i_ = self.weights_of_max_features  # количество критериев большое
    #
    #     i = 0
    #     while i < self.features_quantity:
    #         alternatives_i = self.get_weights_list(alternatives_answers[i])
    #         alternatives.append(alternatives_i)
    #         i = i + 1
    #
    #     # sum(ak*fk(xi))
    #     J = np.zeros(self.alternatives_quantity)
    #     i = 0
    #     j = 0
    #     while i < self.alternatives_quantity:
    #         while j < self.features_quantity:
    #             J[i] = J[i] + (alternatives[j][i] * features_i_[j])
    #             j = j + 1
    #         i = i + 1
    #         j = 0
    #     max_ = J.argmax()
    #     print("alternatives: ", alternatives)
    #
    #     i = 0
    #     all_datasets = []
    #     while i < self.alternatives_quantity:
    #         alternative = []
    #         j = 0
    #         while j < self.features_quantity:
    #             alternative.append(alternatives[j][i])
    #             j = j + 1
    #         all_datasets.append(alternative)
    #         i = i + 1
    #     print("all_datasets: ", all_datasets)
    #     print("J: ", J.tolist())
    #
    #     result = {"best": int(max_ + 1), "all_datasets": all_datasets, "functional": J.tolist()}
    #     result = json.dumps(result)
    #     return result

    def get_best_alternative_djoffrion(self):
        """

        :return:
        """
        alternatives_answers = self.answers_about_alternatives
        # self.get_additional_question()
        alternatives = []
        if len(self.weights_of_max_features) == 0:  # это значит что количество критериев небольшое
            features_answers = self.answers_about_features
            features_i_ = get_weights_list(features_answers)
        else:
            features_i_ = self.weights_of_max_features  # количество критериев большое

        for i, _ in enumerate(self.features_names):
            alternatives_i = get_weights_list(alternatives_answers[i])
            alternatives.append(alternatives_i)

        Ta = np.zeros(self.alternatives_quantity)
        Ta_min = np.zeros(self.features_quantity)
        for i, _ in enumerate(self.alternatives_names):
            for j, _ in enumerate(self.features_names):
                Ta_min[j] = (alternatives[j][i] * features_i_[j])
            Ta[i] = min(Ta_min)

        # min(ai*fi(x)) -> max J(xi)
        max_ = max(Ta)
        best_variants = []

        i = 0
        for each in Ta:
            if each == max_:
                best_variants.append(i)
            else:
                best_variants.append(-1)
            i = i + 1

        # sum(ak*fk(xi))
        J = np.zeros(len(best_variants))
        j = 0
        for i, _ in enumerate(best_variants):
            if best_variants[i] != -1:
                while j < self.features_quantity:
                    J[i] = J[i] + (alternatives[j][best_variants[i]] * features_i_[j])
                    j = j + 1
            j = 0

        max_ = J.argmax()
        i = 0
        all_datasets = []
        while i < self.alternatives_quantity:
            alternative = []
            j = 0
            while j < self.features_quantity:
                alternative.append(alternatives[j][i])
                j = j + 1
            all_datasets.append(alternative)
            i = i + 1
        print("all_datasets: ", all_datasets)
        print("J: ", Ta.tolist())
        result = {"best": int(max_), "all_datasets": all_datasets, "functional": Ta.tolist(),
                  "indexes": self.features_names}
        result = json.dumps(result)
        return result
