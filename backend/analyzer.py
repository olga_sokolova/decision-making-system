"""Module for inflection of words and formation of questions"""
import random
import pymorphy2


def do_feature_title(feature_i_parse, feature2_gent_long, case):
    """

    :param feature_i_parse:
    :param feature2_gent_long:
    :param case:
    :return:
    """
    feature2_gent = feature_i_parse.inflect({case})
    if feature2_gent is not None:
        feature2_gent_long += feature2_gent[0].title() + " "
    else:
        feature2_gent_long += feature_i_parse.title() + " "
    return feature2_gent_long


def do_single_feature_title(feature, feature2_begin, case):
    """

    :param feature:
    :param feature2_begin:
    :param case:
    :return:
    """
    feature2_begin_ = feature.inflect({case})
    if feature2_begin_ is not None:
        feature2_begin_gent = feature2_begin_[0].title()
    else:
        feature2_begin_gent = feature2_begin.title()
    return feature2_begin_gent


def check_title(flag_is_title, feature_i_parse, feature2_gent_long, case):
    """

    :param flag_is_title:
    :param feature_i_parse:
    :param feature2_gent_long:
    :param case:
    :return:
    """
    if flag_is_title:
        feature2_gent_long = do_feature_title(feature_i_parse, feature2_gent_long, case)
        flag_is_title = False
    else:
        feature2_gent = feature_i_parse.inflect({case})
        if feature2_gent is not None:
            feature2_gent_long += feature2_gent[0] + " "
        else:
            feature2_gent_long += feature_i_parse + " "
    return feature2_gent_long, flag_is_title


class WordsAnalyzer:
    """Class for analyzing words"""
    morph = pymorphy2.MorphAnalyzer()

    def inflect_not_noun(self, feature2_begin, case, flag_is_title):
        """

        :param feature2_begin:
        :param case:
        :param flag_is_title:
        :return:
        """
        if flag_is_title:
            feature2_begin_gent = do_single_feature_title(self.morph.parse(feature2_begin)[0],
                                                          feature2_begin, case)
        else:
            feature2_begin_ = self.morph.parse(feature2_begin)[0].inflect({case})
            if feature2_begin_ is not None:
                feature2_begin_gent = feature2_begin_[0]
            else:
                feature2_begin_gent = feature2_begin
        return feature2_begin_gent

    def inflect_noun(self, data, case, flag_is_title):
        """

        :param data:
        :param case:
        :param flag_is_title:
        :return:
        """
        i = 0
        feature2_gent_long = ""
        while i < len(data):
            feature_i_parse = self.morph.parse(data[i])[0]

            if feature_i_parse.tag.POS == "ADJF":
                feature2_gent_long, flag_is_title = check_title(flag_is_title, feature_i_parse,
                                                                feature2_gent_long, case)
                i += 1
            else:
                feature_i_parse = self.morph.parse(data[i])[0].inflect({case})
                if feature_i_parse is not None:
                    feature2_gent_long += feature_i_parse[0]
                else:
                    feature2_gent_long += data[i]
                i += 1
                break

        # add rest of feature 2
        while i < len(data):
            feature2_gent_long += " " + data[i]
            i += 1
        feature2_gent = feature2_gent_long
        return flag_is_title, feature2_gent

    def get_word_on_the_case(self, feature, case):
        """
        Inflection feature for the case
        :param feature:
        :param case:
        :return:
        """
        flag_is_title = False
        data = feature.split()
        if len(data) > 1:
            feature2_begin = feature[: feature.find(" ")]
            feature2_rest = feature[feature.find(" ") + 1:]
            if feature2_begin.istitle():
                flag_is_title = True
            if self.morph.parse(feature2_begin)[0].tag.POS == "ADJF":
                flag_is_title, feature2_gent = self.inflect_noun(data, case, flag_is_title)
            else:
                feature2_begin_gent = self.inflect_not_noun(feature2_begin, case, flag_is_title)
                feature2_gent = feature2_begin_gent + " " + feature2_rest

        else:
            if feature.istitle():
                flag_is_title = True
            feature2_gent = self.morph.parse(feature)[0].inflect({case})

            if feature2_gent is not None:
                feature2_gent = feature2_gent[0]
            else:
                feature2_gent = feature
            if flag_is_title:
                feature2_gent = feature2_gent.title()
        return feature2_gent

    def get_random_feature_question(self, feature1, feature2):
        """
        Generate random question from templates with two features
        :param feature1:
        :param feature2:
        :return:
        """
        questions = []
        feature2_gent = self.get_word_on_the_case(feature2, 'gent')
        questions.append(f"Оцените, насколько {feature1} для Вас важнее {feature2_gent}")
        questions.append(f"Для вас {feature1} важнее {feature2_gent}? Насколько?")
        questions.append(f"Дайте оценку: насколько {feature1} важнее для вас, чем {feature2}")
        return random.choice(questions)

    def get_random_alternative_question(self, alternative1, alternative2, feature):
        """
        Generate random question from templates with two alternatives
        :param alternative1:
        :param alternative2:
        :param feature:
        :return:
        """
        questions = []
        alternative1_gent = self.get_word_on_the_case(alternative1, 'gent')
        alternative2_gent = self.get_word_on_the_case(alternative2, 'gent')
        feature_gent = self.get_word_on_the_case(feature, 'gent')
        feature_datv = self.get_word_on_the_case(feature, 'datv')
        alternative2_accs = self.get_word_on_the_case(alternative2, 'accs')
        questions.append(f"Оцените, насколько {feature} {alternative1_gent} для Вас предпочтительнее "
                         f"{feature_gent} {alternative2_gent}")
        questions.append(f"Насколько по вашему мнению {alternative1} превосходит {alternative2_accs} "
                         f"по {feature_datv}")
        questions.append(f"Для Вас {feature} {alternative1_gent} предпочтительнее {feature_gent} {alternative2_gent}? "
                         f"Насколько?")
        return random.choice(questions)
